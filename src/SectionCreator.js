import { addJoinUsSection } from './join-us-section';
import { addCommunitySection } from './community-section';
import { removeSection } from './utils';
import {
  JOIN_PROGRAM_HEADER,
  JOIN_PROGRAM_HEADER_ADVANCED,
  JOIN_PROGRAM_BUTTON_SUBSCRIBE,
  JOIN_PROGRAM_BUTTON_SUBSCRIBE_ADVANCED
} from './constants/text-constants';
import { COMMUNITY_SECTION, JOIN_PROGRAM_SECTION } from './constants/selector-constants';

export class SectionCreator {
  create(type) {
    switch (type) {
      case 'advanced':
        return new JoinUsSection(JOIN_PROGRAM_HEADER_ADVANCED, JOIN_PROGRAM_BUTTON_SUBSCRIBE_ADVANCED);
      case 'standard':
        return new JoinUsSection(JOIN_PROGRAM_HEADER, JOIN_PROGRAM_BUTTON_SUBSCRIBE);
      case 'community':
        return new CommunitySection();
    }
  }
}

class Section {
  constructor(renderSection, className) {
    this.renderSection = renderSection;
    this.className = className;
  }

  render() {
    this.renderSection();
  }

  remove() {
    removeSection(this.className);
  }
}

class JoinUsSection extends Section {
  constructor(headerText, buttonText) {
    super(() => addJoinUsSection(headerText, buttonText), JOIN_PROGRAM_SECTION);
  }
}

class CommunitySection extends Section {
  constructor() {
    super(addCommunitySection, COMMUNITY_SECTION);
  }
}
