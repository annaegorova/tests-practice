import { addNewElement, createElement, makeRequest } from './utils';
import {
  APP_CONTAINER, APP_SECTION,
  APP_SUBTITLE,
  APP_TITLE,
  COMMUNITY_SECTION,
  COMMUNITY_SECTION_CONTAINER,
  COMMUNITY_SECTION_MEMBERS,
  COMMUNITY_SECTION_MEMBERS_AVATAR,
  COMMUNITY_SECTION_MEMBERS_DESCRIPTION,
  COMMUNITY_SECTION_MEMBERS_NAME,
  COMMUNITY_SECTION_MEMBERS_POSITION,
  CULTURE_SECTION
} from './constants/selector-constants';
import {
  COMMUNITY_SECTION_DESCRIPTION,
  COMMUNITY_SECTION_HEADER,
  COMMUNITY_SECTION_MEMBER_INFO
} from './constants/text-constants';

export function addCommunitySection() {
  const section = createElement('section', [APP_SECTION, COMMUNITY_SECTION]);
  addNewElement(section, 'h2', APP_TITLE, COMMUNITY_SECTION_HEADER);
  addNewElement(section, 'h3', APP_SUBTITLE, COMMUNITY_SECTION_DESCRIPTION);

  section.appendChild(createAvatarContainer());

  const app = document.getElementById(APP_CONTAINER);
  const cultureSection = document.getElementsByClassName(CULTURE_SECTION)[0];
  app.insertBefore(section, cultureSection);
}

function createAvatarContainer() {
  const container = createElement('div', [COMMUNITY_SECTION_CONTAINER]);
  makeRequest('http://localhost:8080/api/community', 'GET')
      .then(response => response.responseData)
      .then(data => data.forEach(el => {
        const memberPane = createElement('div', [COMMUNITY_SECTION_MEMBERS]);
        const avatar = createElement('img', [COMMUNITY_SECTION_MEMBERS_AVATAR]);
        avatar.setAttribute('src', el.avatar);
        memberPane.appendChild(avatar);
        container.appendChild(memberPane);
        addNewElement(memberPane, 'div', [COMMUNITY_SECTION_MEMBERS_DESCRIPTION],
            COMMUNITY_SECTION_MEMBER_INFO);
        addNewElement(memberPane, 'div', [COMMUNITY_SECTION_MEMBERS_NAME],
            `${el.firstName} ${el.lastName}`);
        addNewElement(memberPane, 'div', [COMMUNITY_SECTION_MEMBERS_POSITION], el.position);
      }));
  return container;
}
