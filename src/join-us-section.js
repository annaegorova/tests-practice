import { validate } from './email-validator';
import { addNewElement, createElement, makeRequest } from './utils';
import {
  JOIN_PROGRAM_BUTTON_SUBSCRIBE,
  JOIN_PROGRAM_BUTTON_UNSUBSCRIBE,
  JOIN_PROGRAM_EMAIL_PLACEHOLDER,
  JOIN_SECTION_DESCRIPTION
} from './constants/text-constants';
import {
  APP_CONTAINER,
  APP_FOOTER,
  APP_SECTION,
  APP_SECTION_BUTTON,
  APP_SUBTITLE,
  APP_TITLE,
  JOIN_PROGRAM_SECTION,
  JOIN_PROGRAM_SECTION_BUTTON,
  JOIN_PROGRAM_SECTION_EMAIL,
  JOIN_PROGRAM_SECTION_FORM
} from './constants/selector-constants';

export function addJoinUsSection(headerText, buttonText) {
  const section = createElement('section', [APP_SECTION, JOIN_PROGRAM_SECTION]);
  addNewElement(section, 'h2', APP_TITLE, headerText);
  addNewElement(section, 'h3', APP_SUBTITLE, JOIN_SECTION_DESCRIPTION);

  section.appendChild(createJoinUsSectionForm(buttonText));

  const app = document.getElementById(APP_CONTAINER);
  const footer = document.getElementsByClassName(APP_FOOTER)[0];
  app.insertBefore(section, footer);
}

function createJoinUsSectionForm(submitButtonText) {
  const form = createElement('form', JOIN_PROGRAM_SECTION_FORM);
  const userSubscribed = isSubscribedToProgram();
  const input = createJoinUsFormInput(userSubscribed);
  const button = createJoinUsFormSubmitButton(userSubscribed, submitButtonText);
  form.appendChild(input);
  form.appendChild(button);

  form.addEventListener('submit', e => {
    e.preventDefault();
    if (isSubscribedToProgram()) unsubscribeFromProgram(button, input);
    else validateAndSubscribeToProgram(button, input);
  });

  return form;
}

function isSubscribedToProgram() {
  return localStorage.getItem('subscribedToProgram') === 'true';
}

function toggleSubscription(value) {
  localStorage.setItem('subscribedToProgram', value);
}

function getEmailFromStorage() {
  return localStorage.getItem('subscriptionEmail');
}

function addEmailToStorage(email) {
  localStorage.setItem('subscriptionEmail', email);
}

function removeEmailFromStorage() {
  localStorage.removeItem('subscriptionEmail');
}
function validateAndSubscribeToProgram(button, input) {
  if (validate(input.value)) {
    toggleSubscription(true);
    disableButton(button);
    makeRequest('http://localhost:8080/api/subscribe', 'POST', { email: input.value })
        .then(() => enableButton(button));
    removeEmailFromStorage();
    button.textContent = JOIN_PROGRAM_BUTTON_UNSUBSCRIBE;
    input.classList.add('hidden');
  }
}

function unsubscribeFromProgram(button, input) {
  toggleSubscription(false);
  removeEmailFromStorage();
  disableButton(button);
  makeRequest('http://localhost:8080/api/unsubscribe', 'POST', { email: input.value })
      .then(() => enableButton(button));
  button.textContent = JOIN_PROGRAM_BUTTON_SUBSCRIBE;
  input.value = '';
  input.classList.remove('hidden');
}

function createJoinUsFormSubmitButton(userSubscribed, submitButtonText) {
  const buttonText = userSubscribed ? JOIN_PROGRAM_BUTTON_UNSUBSCRIBE : submitButtonText;
  const button = createElement('button', [APP_SECTION_BUTTON, JOIN_PROGRAM_SECTION_BUTTON], buttonText);

  return button;
}

function createJoinUsFormInput(userSubscribed) {
  const inputClasses = userSubscribed ?
    [JOIN_PROGRAM_SECTION_EMAIL, 'hidden'] :
    JOIN_PROGRAM_SECTION_EMAIL;
  const input = createElement('input', inputClasses);
  input.setAttribute('type', 'text');
  input.setAttribute('placeholder', JOIN_PROGRAM_EMAIL_PLACEHOLDER);
  input.value = getEmailFromStorage();
  input.addEventListener('input', e => {
    addEmailToStorage(e.target.value);
  });

  return input;
}

function disableButton(button) {
  button.setAttribute('disabled', 'true');
  button.classList.add('disabled');
}

function enableButton(button) {
  button.removeAttribute('disabled');
  button.classList.remove('disabled');
}
